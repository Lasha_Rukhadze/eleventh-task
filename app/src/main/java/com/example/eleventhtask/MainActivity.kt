package com.example.eleventhtask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.eleventhtask.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private var countries = mutableListOf<MyCountryModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initializer()

        CoroutineScope(Dispatchers.IO).launch {
            getCountries()
            CoroutineScope(Dispatchers.Main).launch {
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun defaultSetter() : MutableList<MyCountryModel>{
        val defaultList = mutableListOf<MyCountryModel>()
        defaultList.add(MyCountryModel("Georgia", "ge", "Tbilisi"))
        defaultList.add(MyCountryModel("Germany", "de", "Berlin"))
        defaultList.add(MyCountryModel("United Kingdom", "uk", "London"))
        defaultList.add(MyCountryModel("Greece", "gr", "Athens"))
        defaultList.add(MyCountryModel("Italy", "it", "Rome"))
        return defaultList
    }

    private suspend fun getCountries() {
        val response = RetrofitService.retrofitService().getCountry()

        if (response.isSuccessful) {
            countries.addAll(response.body()!!)
        } else {
            countries.addAll(defaultSetter())
        }
    }

    private fun initializer(){
        adapter = RecyclerViewAdapter(countries)
        binding.recycler.layoutManager = LinearLayoutManager(this)
        binding.recycler.adapter = adapter
    }
}