package com.example.eleventhtask

data class MyCountryModel(var name : String,
                          var topLevelDomain : String,
                          var capital : String) {

}
