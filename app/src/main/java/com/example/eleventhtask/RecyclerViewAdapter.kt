package com.example.eleventhtask

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.eleventhtask.databinding.CountriesBinding

class RecyclerViewAdapter (val countries : MutableList<MyCountryModel>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = CountriesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int {
        return countries.size
    }

    inner class ItemViewHolder(private val binding : CountriesBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var model : MyCountryModel
        fun bind() {
            model = countries[adapterPosition]
            binding.name.text = model.name
            binding.topLevelDomain.text = model.topLevelDomain
            binding.capital.text = model.capital
        }
    }
}